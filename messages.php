<?php
// Common Messages	
		$sucessmessage[1] = "Agregado exitosamente";
		$sucessmessage[2] = "Actualizado con éxito";
		$sucessmessage[3] = "Borrado exitosamente";
		$sucessmessage[4] = "Registrado correctamente";
		$sucessmessage[5] = "Estado cambiado exitosamente";		
		$sucessmessage[6] = "Importado exitosamente";
		$sucessmessage[7] = "Correo enviado con éxito a usuarios seleccionados";
		$sucessmessage[8] = "Gracias por registrarte ";
		$sucessmessage[9] = "Gracias por su solicitud. Alguien de nuestro equipo de soporte se pondrá en contacto con usted dentro de las 12-24 horas.";
		$sucessmessage[10] = "Agradecemos sus comentarios. Nuestro equipo de soporte se pondrá en contacto con usted dentro de las 12-24 horas.";
		$sucessmessage[11] = "Gracias por su solicitud. El nombre de usuario y la contraseña se enviarán a su correo electrónico.";		
		$sucessmessage[12] = "Has cambiado correctamente tus detalles de inicio de sesión. Por favor revise su correo electrónico.";
		$sucessmessage[13] = "Gracias por registrarse con nosotros. <br/> Una vez que su cuenta esté activada, recibirá un correo electrónico de nuestra parte.";
		$sucessmessage[14] = "Gracias por su solicitud. Alguien de nuestro equipo se pondrá en contacto con usted dentro de las 12-24 horas.";
		$sucessmessage[15] = "Ingreso invalido";
		$sucessmessage[16] = "Salió exitosamente";
		$sucessmessage[17] = "Base de datos exportada con éxito";	
        $sucessmessage[18] = "Nombre de usuario no válido";	
		$sucessmessage[19] = "Gracias por enviar el formulario de consulta. Tus datos han sido recibidos. Nuestra oficina administrativa se pondrá en contacto con usted pronto.";	
		$sucessmessage[20] = "Año académico creado con éxito Por favor, inicie sesión.";
?>
