<script type="text/javascript" src="includes/js/leftmenu/jquery_google.js"></script>
<script type="text/javascript" src="includes/js/leftmenu/ddaccordion.js"></script>
<script type="text/javascript">
ddaccordion.init({
	headerclass: "expandable", //Shared CSS class name of headers group that are expandable
	contentclass: "categoryitems", //Shared CSS class name of contents group
	revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
	mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false
	defaultexpanded: [0], //index of content(s) open by default [index1, index2, etc]. [] denotes no content
	onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
	animatedefault: false, //Should contents open by default be animated into view?
	persiststate: true, //persist state of opened contents within browser session?
	toggleclass: ["", "openheader"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["prefix", "", ""], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit:function(headers, expandedindices){ //custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
		//do nothing
	}
})
</script>
<style type="text/css">
.arrowlistmenu{
width: 160px; /*width of accordion menu*/
}
.arrowlistmenu .menuheader{ /*CSS class for menu headers in general (expanding or not!)*/
font: bold 10px Arial, Helvetica, sans-serif;
color: #000000;
/*background:url(js/titlebar.png);*/
/*background: black url(titlebar.png) repeat-x center left;*/
/*background-color:#999999;*/
margin-bottom: 2px; /*bottom spacing between header and rest of content*/
text-transform: uppercase;
padding: 3px 0 0px 0px; /*header text is indented 10px*/
cursor: hand;
cursor: pointer;
font-size: 100%;
}
.arrowlistmenu .openheader{ /*CSS class to apply to expandable header when it's expanded*/
/*background-image: url(js/titlebar-active.png);*/
/*background-color:#555B5C;*/
}
.arrowlistmenu ul{ /*CSS for UL of each sub menu*/
list-style-type: none;
margin: 0;
padding: 0;
margin-bottom: 2px; /*bottom spacing between each UL and rest of content*/
}
.arrowlistmenu ul li{
padding-bottom: 2px; /*bottom spacing between menu items*/
color: #000000;
display: block;
padding: 2px 0;
padding-left: 12px;
text-decoration: none;
font-size: 90%;
font: bold 12px Arial, Helvetica, sans-serif;
}
.arrowlistmenu ul li a{
color: #000000;
display: block;
padding: 0 0;
padding-left: 20px; /*link text is indented 19px*/
text-decoration: none;
font-size: 90%;
font: normal 12px Arial, Helvetica, sans-serif;
}
.arrowlistmenu ul li a:visited{
/*color: #555B5C;*/
}
.arrowlistmenu ul li a:hover{ /*hover state CSS*/
color: #990000;
}
.mainsidelink {
color: #000000;
text-decoration:none;
}

.mainsidelink :hover{
color: #000000;
text-decoration:none;
}
</style>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="35" align="center" class="left_admin">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><div class="arrowlistmenu">

<?php
// top permitions from apex

$edit_mod = $db->getRow("SELECT * FROM es_modules_alloted  WHERE id=1");
$max_students=$edit_mod['max_no_students'];
$max_courses=$edit_mod['max_no_courses'];
$modules_permissions=$edit_mod['modules_permissions'];

$top_level_permissions= explode(',', $modules_permissions);

$admin_permissions = explode(',', $permissions['admin_permissions']);

				?>
<div class="menuheader expandable">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/birth_32.png" /></td>
    <td>
Cumpleaños</td>
  </tr>
</table>
</div>
<ul class="categoryitems">
<li><a href="javascript:void(0)" onclick="window.open('?pid=44&action=birthday_students',null,'width=700,height=600,scrollbars=yes,toolbar=no,directories=no,status=no,menubar=yes,left=140,top=30');">
Cumpleaños de estudiantes</a></li>
</ul>



<?php
					if (in_array('1_p', $top_level_permissions) ){
	if (in_array('1_p', $admin_permissions) ){?>
<div class="menuheader expandable">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/administrator_32.png" /></td>
    <td>
Administración</td>
  </tr>
</table>
</div>
<ul class="categoryitems">
<li><a href="?pid=42&action=adminlist">
Lista de administrador</a></li>
<?php if (in_array("1_3", $admin_permissions)) {?><li><a href="?pid=42&action=addadmin">
Agregar administrador</a></li><?php }?>
</ul>
<?php } }
if (in_array('2_p', $top_level_permissions) ){
if (in_array('2_p', $admin_permissions)){?>

<div class="menuheader expandable">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/setup_32.png" /></td>
    <td > 
Reparar</td>
  </tr>
</table></div>
<ul class="categoryitems">
<li><a href="?pid=22&action=school_details">Detalles del instituto</a></li>
<li><a href="?pid=20&action=manageclasses">
Grupos/Clases/Asignaturas</a></li>
<li><a href="?pid=97&action=list">
Secciones de clase</a></li>
<li><a href="?pid=20&action=htmlcode">
API para iniciar sesión</a></li>
<!--<li><a href="?pid=121&page=castecategory">Caste </a></li>-->
<li><a href="?pid=94&page=caste">
Categorías de castas</a></li>
<!--<li><a href="?pid=121&page=cat">Categories & Caste </a></li>-->
<!--<li><a href="?pid=94&page=int">Other Institutes</a></li>-->
<li><a href="?pid=94&page=transport">
Punto de recogida de estudiantes </a></li>
<!--<li><a href="?pid=94&page=subject&action=list">Subjects Categorization</a></li>-->
</ul>
<?php }?>
<?php }
// Certificates
if (in_array('35_p', $top_level_permissions) ){if (in_array('35_p', $admin_permissions)){?>
<div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/certi.png" width="31" height="44" /></td>
    <td >
Certificados</td>
  </tr>
</table></div>
<ul class="categoryitems">
<li><a href="?pid=117&action=list">
Certificado Bonafied</a></li>
<!--<li><a href="?pid=120&action=list">Bonafied for Bank_Acount</a></li>
<li><a href="?pid=119&action=list">Bonafied for IncomeTax Rebate</a></li>-->
<? //if (in_array('5_6', $admin_permissions)){?>
<li><a href="?pid=95&action=list"><span id="internal-source-marker_0.052443267584382114">Personaje</span> 
Certificado</a></li>

<?php //} ?>
<li><a href="?pid=116&action=list">
Carta de experiencia</a></li>
<li><a href="?pid=95&action=attemptlist">
Certificado de intento</a></li>
<li><a href="?pid=95&action=feelist">Aviso de tarifas</a></li>
<li><a href="?pid=95&action=undertakinglist"> 
Empresa carta</a></li>
<li><a href="?pid=95&action=holilist">
Aviso de vacaciones</a></li>
<li><a href="?pid=95&action=eligibilitylist"> 
Certificado de elegibilidad</a></li>
<li><a href="?pid=95&action=absentlist"> 
Aviso de ausencia del estudiante</a></li>
<!--<li><a href="?pid=118&action=list">Loan Certificate</a></li>-->

</ul>

 <?php } // end of certificate ?>


 <?php }
 // Government
 /*?>if (in_array('35_p', $top_level_permissions) ){if (in_array('35_p', $admin_permissions)){?>
<div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/government_icon.jpg" width="30" height="35" /></td>
    <td >Government Rules</td>
  </tr>
</table></div>
<ul class="categoryitems">
<li><a href="?pid=122&action=list">Association Executive Committee </a></li>
<!--<li><a href="?pid=120&action=list">Bonafied for Bank_Acount</a></li>
<li><a href="?pid=119&action=list">Bonafied for IncomeTax Rebate</a></li>-->
<? //if (in_array('5_6', $admin_permissions)){?>
<li><a href="?pid=123&action=list">School Committee</a></li>
<?php //} ?>
<li><a href="?pid=126&action=list">Academic Council</a></li>
<li><a href="?pid=128&action=list">Meeting</a></li>
<li><a href="?pid=130&action=list">School Year Planning</a></li>
</ul><?php */?>

 <?php //}//} // end of certificate ?>

<?php

if (in_array('3_p', $top_level_permissions) ){
if (in_array('3_p', $admin_permissions)){ ?>
<div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/frontoffice_32.png" /></td>
    <td >
Oficina frontal</td>
  </tr>
</table></div>
<ul class="categoryitems">
<?php if (in_array('3_1', $admin_permissions)){ ?><li><a href="?pid=8">
Formulario de Consulta</a></li><?php }?>
<li><a href="?pid=2&action=list_enquiry">
Lista de consultas</a></li>
<?php //if (in_array("3_4", $admin_permissions)) {?><!--<li><a href="?pid=3&action=list_enquiry">Admitted Students</a></li>--><?php //}?>
<?php ?><?php if (in_array("3_7", $admin_permissions)) {?><li><a href="?pid=3&action=enquiry_students">
Estudiantes admitidos [Investigación]</a></li><?php }?><?php ?>

 <!--<li> <a href="report.html"class="menutitlein">Reports</a><br /> </li>-->
<!--<li><a href="?pid=3">Entrance Test</a></li>-->
</ul>
<?php }

 }
 if (in_array('4_p', $top_level_permissions) ){if (in_array('4_p', $admin_permissions)){?>
<div class="menuheader" style="cursor: default">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td width="37px"><img src="images/preadmission_32.png" /></td>
    <td ><a href="?pid=5&action=pre_admission" class="mainsidelink">
Formulario de Admisión</a></td>
  </tr>
  <!--<li><a href="?pid=107&action=feedet">New Registration Form</a></li>-->
</table></div>
<?php } }if (in_array('5_p', $top_level_permissions) ){if (in_array('5_p', $admin_permissions)){?>
<div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/student_32.png" /></td>
    <td > Estudiante</td>
  </tr>
</table></div>
<ul class="categoryitems">

<li><a href="?pid=96&action=assign_section">
Secciones/Números de Rollo</a></li>
<li><a href="?pid=21&action=serchclass">
Buscar registro de estudiante</a></li>

<li><a href="?pid=21&action=classrecards">
Actualizar registro de clase</a></li>
<li><a href="?pid=21&action=malefemalestudents">
Macho/Femenino</a></li>
<?php if (in_array('5_5', $admin_permissions)){?>
<li><a href="?pid=23&action=issuetcforstudent">
Estudiantes transferidos</a></li>
<?php } ?>

<li><a href="?pid=3&action=cast_list">
Categoría&nbsp;Sabia&nbsp;Datos </a></li>
<li><a href="?pid=3&action=age_wise">
Años&nbsp;Sabia&nbsp;Datos</a></li>

<li><a href="?pid=21&action=studentlist2">Estudiantes&nbsp;Lista&nbsp;</a></li>
</ul>
<?php }}if (in_array('6_p', $top_level_permissions) ){if (in_array('6_p', $admin_permissions)){?>
<!--div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/feepayment_32.png" /></td>
    <td >
Pago de honorarios</td>
  </tr>
</table></div>
<ul class="categoryitems">
<li><a href="?pid=17&action=createfeetypes" >
Agregar tarifas </a></li-->
<!--<li><a href="?pid=79&action=addfine"><span id="internal-source-marker_0.5963001342130408">Late Fee Fine</span></a></li>-->
<!--<li><a href="?pid=79&action=add_lastdate">Fee Due Date</a></li>-->
<!--li><a href="?pid=17&action=viewfees">
Detalles de tarifas</a></li>
<?php /*?><li><a href="?pid=105&action=add_examfee"> Add Exam Fee</a></li>
<?php if(in_array('6_20',$admin_permissions)){?>
<li><a href="?pid=105&action=view_list">Exam Fee Collection</a></li>
<?php } ?>
<?php */?><li><a href="?pid=40&action=payfee">Cuota de pago</a></li>
<li><a href="?pid=40&action=receipt_list">
Imprima el recibo</a></li-->
<!--<li><a href="?pid=40&action=fee_card">Student Fee Card</a></li>-->
<!--li><a href="?pid=40&action=ad_fee_card">
Tarjeta de tarifa de estudiante</a></li-->
<!--<li><a href="?pid=40&action=classwise_fee_card">Class Wise Fee Status</a></li>-->
<!--li><a href="?pid=40&action=ad_classwise_fee_card">Estado de tarifa de clase sabia</a></li>
<li><a href="?pid=40&action=feepaidlist&pre_class=ALL">
Lista de tarifas pagadas</a></li>
<li><a href="?pid=40&action=fee_paid_list">
Categoría Salario pagado</a></li-->
<!--<li><a href="?pid=40&action=classwisepayment_list">Paid Fee [Class wise]</a></li>
<li><a href="?pid=40&action=categorywisefee">Category Wise Details</a></li>
<li><a href="?pid=40&action=outstandingfees&pre_class=ALL">Outstanding Fees</a></li>
<li><a href="?pid=40&action=installment_fines">Late Fee Paid</a></li>
<?php //if(in_array('6_14',$admin_permissions)){?>
<li><a href="?pid=79&action=add_otherfines"> Add Misc. Fine</a></li>
<?php //}?>
<li><a href="?pid=79&action=view_list">Misc Fine Collection</a></li>
<li><a href="?pid=79&action=view_oldbalances">View Old Balances</a></li>-->
</ul>

<?php }}if (in_array('7_p', $top_level_permissions) ){if (in_array('7_p', $admin_permissions)){?>

<!--div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/assignment_32.png" /></td>
    <td >
Asignación</td>
  </tr>
</table></div>
<ul class="categoryitems">
<li><a href="?pid=4&action=addassignment">
Agregar asignación</a></li>
<li><a href="?pid=4&action=view">
Ver asignación</a></li>
</ul>

<?php }}if (in_array('8_p', $top_level_permissions) ){ if (in_array('8_p', $admin_permissions)){?>
<div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/tutorials_32.png" /></td>
    <td >
Material de estudio </td>
  </tr>
</table></div>
<ul class="categoryitems">
<li><a href="?pid=51&action=list">Añadir&nbsp;
Unidades</a></li>
<li><a href="?pid=59&action=list">Añadir&nbsp;
Temas</a></li>
<li><a href="?pid=60&action=list">Añadir Tutoriales</a></li>
<li><a href="?pid=61&action=list">Añadir 
Folleto</a></li>
<li><a href="?pid=63&action=list">
Banco de preguntas</a></li>
</ul>
<?php }}if (in_array('10_p', $top_level_permissions) ){if (in_array('10_p', $admin_permissions)){?>
<div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/staff_32.png" /></td>
    <td>PERSONAL
</td>
  </tr>
</table></div>
<ul class="categoryitems">

<li><a href="?pid=49&action=department">
Agregar departamentot</a></li>
<li><a href="?pid=46&action=addnewstaff">
Agregar personal</a></li>
<li><a href="?pid=15&action=staffviewing">
Ver personal</a></li>
<li><a href="?pid=64&action=asignincharge">
Asignar carga</a></li>
</ul>

<?php }}if (in_array('9_p', $top_level_permissions) ){if (in_array('9_p', $admin_permissions)){?>
<div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/hrd_32.png" /></td>
    <td >HRD</td>
  </tr>
</table></div>
<ul class="categoryitems">
<li><a href="?pid=9&action=post_vacancy">
Publicar vacante</a></li>
<li><a href="?pid=9&action=list_classifieds">
Clasificados</a></li>
<li><a href="?pid=23&action=applicant_enquiries">
Consulta del solicitante</a></li>
<li><a href="?pid=23&action=search_applicants">
Buscar solicitantes</a></li>
<li><a href="?pid=23&action=take_interview">Tomar la entrevista</a></li>
<li><a href="?pid=15&action=applicants_list">
Lista de solicitantes</a></li>
<li><a href="?pid=23&action=offerlettergenration">
Generar carta de oferta</a></li>
<li><a href="?pid=23&action=letter_formats">
Formatos de letras</a></li>
<li><a href="?pid=23&action=issuetcstaff">
Renuncia / Terminación</a></li>
<li><a href="?pid=23&action=letterslist">
Otros formatos de letras</a></li>
<li><a href="?pid=23&action=sendlettertostaff">
Enviar carta</a></li>
<li><a href="?pid=23&action=otherlettergeneration">
Letra impresa</a></li>
</ul>

<?php }}if (in_array('11_p', $top_level_permissions) ){if (in_array('11_p', $admin_permissions)){?>
<div class="menuheader expandable">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/payroll_32.png" /></td>
    <td >
Nómina de sueldos</td>
  </tr>
</table></div>
<ul class="categoryitems">

<li><a href="?pid=29&action=leavemaster">

Crear licencia anuale</a></li>
<li><a href="?pid=29&action=allowencemaster">
Crear tipo de asignación</a></li>
<li><a href="?pid=29&action=deductionsmaster">
Crear tipo de deducción</a></li-->
<!--<li><a href="?pid=29&action=invest"> Create Investment</a></li>-->
<!--li><a href="?pid=29&action=loanmaster">
Crear un préstamo</a></li>
<li><a href="?pid=29&action=taxmaster">
Crear un impuesto</a></li>
<li><a href="?pid=29&action=pfmaster"> 
Crear FP</a></li>

<li><b>Empleado</b></li>
<li><a href="?pid=35&action=issueloan">
Préstamo de emisión</a></li>
<li><a href="?pid=35&action=loanissueslist"><span id="internal-source-marker_0.820553458583017">
Préstamo emitido a</span></a></li>

<li><b>
Generación de recibo de sueldo</b></li>
<li><a href="?pid=35&action=employeewisepayslip">
Recibo de sueldo del empleado</a></li-->
<?php if (in_array("11_103", $admin_permissions)) {?><!--li><a href="?pid=35&action=paysliplist">
Lista de recibo de sueldo</a></li--><?php }?>
<!--<li><a href="?pid=35&action=yearwisepayslip">Year wise pay slip</a></li-->

</ul>
<?php } }if (in_array('12_p', $top_level_permissions) ){if (in_array('12_p', $admin_permissions)){?>
<!--div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/financial_accounting_32.png" /></td>
    <td> 
Contabilidad </td>
  </tr>
</table></div>
<ul class="categoryitems">

<li><a href="?pid=22&action=master_group">Crear&nbsp;
Grupos de cuentas</a></li>
<li><a href="?pid=22&action=ledger">Crear&nbsp;
Cuenta&nbsp;
Libro mayor</a></li>
<li><a href="?pid=22&action=voucher">
Gestionar&nbsp;
Vale</a></li>
<li><b>Transaction</b></li>
<li><a href="?pid=24&action=voucher_entry">
Entrada de comprobante</a></li>
<li><a href="?pid=24&action=vouchers_list">
Lista de cupones</a></li>
<li><b>Report</b></li>
<li><a href="?pid=25&action=balancesheet">
Hoja de balance</a></li>
<li><a href="?pid=25&action=ledger">
Resumen del libro mayor</a></li>
</ul>
<?php } }if (in_array('13_p', $top_level_permissions) ){if (in_array('13_p', $admin_permissions)){?>
<div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/inventory_32.png" /></td>
    <td >
Inventario </td>
  </tr>
</table></div>
<ul class="categoryitems">
<?php /*?><?php if (in_array("13_1", $admin_permissions)) {?>
<li><a href="?pid=7&action=addinventory">Create Inventory Type</a></li>
<?php }?>
<?php */?>

<?php /*?><?php if (in_array("13_1", $admin_permissions)) {?>
<li><a href="?pid=7&action=addmaintain">Maintanance</a></li>
<?php }?>
<?php */?>
<?php if (in_array("13_4", $admin_permissions)) {?>
<li><a href="?pid=7&action=addcategory">
Agregar categoría de producto</a></li>
<?php }?>

<?php if (in_array("13_7", $admin_permissions)) {?>
<li><a href="?pid=7&action=additem">
Añadir artículo</a></li>
<?php }?>

<?php if (in_array("13_10", $admin_permissions)) {?>
<li><a href="?pid=7&action=addsupply">
Añadir proveedor</a></li>
<?php }?>

<?php if (in_array("13_13", $admin_permissions)) {?>
<li><a href="?pid=7&action=add_order">
Orden de compra</a></li>
<?php }?>

<?php if (in_array("13_14", $admin_permissions)) {?>
<li><a href="?pid=7&action=goods_reciept">
Nota de entrada de mercancías</a></li>
<?php }?>

<?php if (in_array("13_15", $admin_permissions)) {?>
<li><a href="?pid=7&action=goods_issue">
Nota de salida de mercancías</a></li>
<?php }?>

<?php if (in_array("13_16", $admin_permissions)) {?>
<li><a href="?pid=7&action=return_issue">
Emitir nota de devolución</a></li>
<?php }?>
<li><a href="?pid=7&action=stock_details">
Detalles de stock</a></li>
<li><a href="?pid=7&action=inventory_reports">
Informes de inventario</a></li>

<?php /*?><li><a href="?pid=99&action=inventory_reports">Stationary Sales</a></li>
<?php if (in_array("13_108", $admin_permissions)) {?>
<li><a href="?pid=103&action=saled_stationary">Stationary&nbsp;Sales&nbsp;Invoices</a></li>
<?php } ?><?php */?>
</ul>


<?php }}if (in_array('14_p', $top_level_permissions) ){if (in_array('14_p', $admin_permissions)){?>
<div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/transport_32.png"  height="32" width="32" border="0"/></td>
    <td >
Transporte </td>
  </tr>
</table></div>
<ul class="categoryitems">
<?php /*?><li><a href="?pid=11&action=allotdriver">Allot Driver</a></li>
<li><a href="?pid=11&action=addtransport">Transport</a></li>
<li><a href="?pid=11&action=maintenance">Maintenance Details</a></li>
<li><a href="?pid=11&action=viewreport">Report</a></li><?php */?>-->
<!--<li><a href="?pid=78&action=vehiclefees" >Vehicle Fees</a></li>-->
<!--li><a href="?pid=75&action=routelist1">
Ruta </a></li>
<li><a href="?pid=75&action=routelist">
Lista de ruta</a></li>
<li><a href="?pid=76&action=boardlist">
Lista de la Junta</a></li>
<li><a href="?pid=77&action=vehiclelist">
Lista de vehiculos</a></li>
<li><a href="?pid=81&action=driverlist">
Lista de conductores</a></li>
<li><a href="?pid=80&action=allottedlist">
Asignar vehículo a bordo</a></li>
<li><a href="?pid=82&action=allotteddriverlist">
Asignar conductor al vehículo</a></li>

<?php if (in_array("14_13", $admin_permissions)) {?>
<li><a href="?pid=83&action=preparetransportbill">
Preparar tarifa de transporte</a></li>
<?php }?>
<li><a href="?pid=84&action=viewtransportbill">
Ver facturas de transporte</a></li-->
<!--<li><a href="?pid=85&action=addmaintenance">Add Maintenance</a></li>-->


<!--<li><a href="?pid=85&action=maintenancedetails">
Detalles de mantenimiento</a></li>


<li><b>
Informes</b></li>
<li><a href="?pid=86&action=driverreport">
Informe del conductor</a></li>
<li><a href="?pid=87&action=vehiclereport">
Informe del vehículo</a></li>
<?php /*?><li><a href="?pid=88&action=studentreport">Student Wise Report</a></li><?php */?>
<li><a href="?pid=88&action=driver_copy">
Informe sabio del estudiante</a></li>
<li><a href="?pid=89&action=staffreport">
Informe sabio del personal</a></li-->
</ul>
<?php } } if (in_array('15_p', $top_level_permissions) ){if(in_array('15_p', $admin_permissions)){?>
<div class="menuheader expandable">
<table width="100%" border="0" cellspacing="0" cellpadding="0"-->
<tr>
    <td width="35px"><img src="images/time_table_32.png" /></td>
    <td >
Tabla de tiempos</td>
  </tr>
</table></div>
<ul class="categoryitems">
<!--<li><a href="?pid=31&action=timetable">Class wise timetables</a></li>
<li><a href="?pid=90&action=timetable">Staff wise timetables</a></li>
<li><a href="?pid=104&action=addtmimes">Period&nbsp;Durations</a></li>
<li><a href="?pid=104&action=timetable">Time&nbsp;Tables</a></li>
<li><a href="?pid=104&action=staff_wise">Staff&nbsp;Time&nbsp;Table</a></li>-->
<li><a href="?pid=106&action=timetable">
Horarios sabios de clase</a></li>
<li><a href="?pid=90&action=staff">
Horarios sabios del personal</a></li>
<li><a href="#" onclick="window.open('?pid=90&action=free_staff')">
Ver personal gratuito</a></li>
</ul>
<?php }}if (in_array('16_p', $top_level_permissions) ){if (in_array('16_p', $admin_permissions)){?>
<!--<div class="menuheader" style="cursor: default">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td width="37px"><img src="images/library_32.png" /></td>
    <td ><a href="?pid=32&action=first" class="mainsidelink">Library</a></td>
  </tr>
</table></div>
<?php }}if (in_array('17_p', $top_level_permissions) ){if (in_array('17_p', $admin_permissions)){?>
<div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/exam_32.png" /></td>
    <td>
Examen</td>
  </tr>
</table></div>
<ul class="categoryitems">
<li><b>
Examen</b></li>
<li><a href="?pid=47&action=manageexams" >
Agregar exámenes</a></li>
<?php if (in_array("17_1", $admin_permissions)) {?>
<li><a href="?pid=36&action=createxam">
Crear examen</a></li>
<?php }?>
<?php if (in_array("17_6", $admin_permissions)) {?>
<li><a href="?pid=36&action=createxamexport">
Examen de exportación</a></li>
<?php }?>
<?php if (in_array("17_2", $admin_permissions)) {?>
<li><a href="?pid=36&action=marksentry">
Marcas Sujetas</a></li>
<?php }?>
<?php if (in_array("17_3", $admin_permissions)) {?>
<li><a href="?pid=36&action=stdmarksentry">
Marcas de estudiante
</a></li>
<?php }?>
<?php if (in_array("17_7", $admin_permissions)) {?>
<li><a href="?pid=36&action=allstudents">Reporte</a></li>
<?php }?>
<?php if (in_array("17_4", $admin_permissions)) {?>
<li><a href="?pid=36&action=allstudentsexport">
Reporte de exportacion</a></li>
<?php }?>
<?php if (in_array("17_5", $admin_permissions)) {?>
<li><a href="?pid=36&action=stud_report">
Informe del alumno</a></li>
<?php }?>
<?php if (in_array("17_8", $admin_permissions)) {?>
<li><a href="?pid=36&action=chatreports">Informe de examen</a></li>
<?php }?>
<?php if (in_array("17_9", $admin_permissions)) {?>
<li><a href="?pid=36&action=chatreports_schoolwise">
Informe del instituto</a></li>
<?php }?>
<li><a href="?pid=100">
Rangos de estudiantes</a></li>
<li><a href="?pid=102"></a></li>
</ul-->
<?php } }if (in_array('18_p', $top_level_permissions) ){if (in_array('18_p', $admin_permissions)){?>
<!--<div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/attendance_32.png" /></td>
    <td >
Asistencia</td>
  </tr>
</table></div>
<ul class="categoryitems">
<li><a href="?pid=27&action=slip">
Boletas de Asistencia</a></li>
<li><a href="?pid=27&action=stud_attend">
Asistencia estudiantil</a></li>
<li><a href="?pid=27&action=edit_stud_attendence">
Editar asistencia</a></li>
<li><a href="?pid=27&action=staff_attend">Asistencia del personal</a></li>
<li><a href="?pid=27&action=edit_staff_attendence">
Editar asistencia</a></li>
<li><a href="?pid=27&action=stud_report">Informe del alumno</a></li>
<li><a href="?pid=27&action=class_report">
Informe de clase</a></li>

<li><a href="?pid=134&action=class-report1">
Informe de clase1</a></li>

<li><a href="?pid=27&action=staff_wise_report">
Informe del empleado</a></li>
<li><a href="?pid=27&action=staff_report">Informe del personal</a></li>
<li><a href="?pid=27&action=descriptive_notes">Notas descriptivas</a></li>
</ul>
<?php }}
if (in_array('19_p', $top_level_permissions) ){if (in_array('19_p', $admin_permissions)){?>
<div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/hostel_32.png" /></td>
    <td >
Hotel</td>
  </tr>
</table></div>
<ul class="categoryitems">
<li><a href="?pid=19&action=addbuilding">Agregar edificio</a></li>
<li><a href="?pid=19&action=addroom">
Añadir habitación</a></li>
<li><a href="?pid=19&action=buildreport">
Disponibilidad de cuarto</a></li>
<li><a href="?pid=19&action=student_roomallotment">
Asignación de habitaciones</a></li>
<li><a href="?pid=19&action=view_persons">
Ver personas del albergue</a></li>
<li><a href="?pid=19&action=collect_items">Recoge artículos</a></li>
<li><a href="?pid=19&action=prepare_bill">
Preparar factura</a></li>
<li><a href="?pid=19&action=viewdetails">
Ver detalles</a></li>
</ul>
<?php }}

if (in_array('20_p', $top_level_permissions) ){if (in_array('20_p', $admin_permissions)){?>
<div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/compose_message_32.png" /></td>
    <td > 
Mensaje </td>
  </tr>
</table></div>
<ul class="categoryitems">
<li><a href="?pid=50&action=mails_received" class="mainsidelink">
Bandeja de entrada de mensajes</a></li>
<li><a href="?pid=50&action=sentmails" class="mainsidelink">
Mensajes enviados</a></li>
<li><b>
Crear Mensaje</b></li>
<?php if (in_array("20_2", $admin_permissions)) {?><li><a href="?pid=50&action=mailtoadmin">
Al administrador</a></li><?php }?>
<?php if (in_array("20_3", $admin_permissions)) {?><li><a href="?pid=50&action=mailtostaff">Al personal</a></li><?php }?>
<?php if (in_array("20_4", $admin_permissions)) {?><li><a href="?pid=50&action=mailtostudents">A los estudiantes</a></li><?php }?>
</ul>
<?php }}if (in_array('32_p', $top_level_permissions) ){if (in_array('32_p', $admin_permissions)){?>
<div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/idcard_32.png" /></td>
    <td > 
Tarjeta de identificación </td>
  </tr>
</table></div>
<ul class="categoryitems">
<li><a href="?pid=72&action=addimage">
Agregar imagen de tarjeta de identificación</a></li>
<?php if (in_array("32_4", $admin_permissions)) {?>
<li><a href="?pid=72&action=mailtostaff">
Personal</a></li>
<?php }?>
<?php if (in_array("32_4", $admin_permissions)) {?>
<li><a href="?pid=72&action=mailtostudents">
Estudiantes</a></li><?php }?>
</ul>
<?php }}

if (in_array('33_p', $top_level_permissions) ){if (in_array('33_p', $admin_permissions)){?>
<div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/frontoffice_32.png" /></td>
    <td > 
Notaria</td>
  </tr>
</table></div>
<ul class="categoryitems">
<li><a href="?pid=74&action=dispatchcategory">
Agregar grupo de despacho</a></li>
<?php if (in_array("33_4", $admin_permissions)) {?>
<li><a href="?pid=74&action=incomingletters">
Entrada de despacho interno / externo</a></li--><?php }?>


<!--<?php //if (in_array("33_5", $admin_permissions)) {?><li><a href="?pid=74&action=outletters">Outward Dispatch Entry</a></li><?php //}?>-->
<!--<li><a href="?pid=74&action=manageletters">
Administrar cartas</a></li>

</ul>
<?php }}if (in_array('21_p', $top_level_permissions) ){if (in_array('21_p', $admin_permissions)){?>
<div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/sms_32.png" /></td>
    <td >SMS</td>
  </tr>
</table></div>
<ul class="categoryitems">
<?php if (in_array("21_1", $admin_permissions)) {?><li><a href="?pid=62&action=smstostaff">
Al personal</a></li><?php }?>
<?php if (in_array("21_2", $admin_permissions)) {?><li><a href="?pid=62&action=smstostudents">
A los estudiantes</a></li><?php }?>
<?php if (in_array("21_1", $admin_permissions)) {?><li><a href="?pid=62&action=smstoall">A todos</a></li><?php }?>
<?php /*?><li><a href="?pid=62&action=balance">Check Balance</a></li><?php */?>
<?php if (in_array("21_3", $admin_permissions)) {?><li><a href="?pid=62&action=enquirylist">
Lista de consultas</a></li><?php }?>
<?php ?><li><a href="?pid=62&action=smssetup">
Preparar SMS</a></li><?php ?>
</ul>
<?php }}if (in_array('22_p', $top_level_permissions) ){if (in_array('22_p', $admin_permissions)){?>
<div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/notice_32.png" /></td>
    <td >
Enviar aviso</td>
  </tr>
</table></div>
<ul class="categoryitems">
<li><a href="?pid=57&action=mails_received" class="mainsidelink">
Respuestas recibidas</a></li>
<li><a href="?pid=57&action=sentmails" class="mainsidelink">
Avisos enviados</a></li>
<?php if (in_array("22_1", $admin_permissions)) {?>
<li><a href="?pid=57&action=mailtostaff">
Al personal</a></li><?php }?>
<?php if (in_array("22_2", $admin_permissions)) {?><li><a href="?pid=57&action=mailtostudents">
A los estudiantes</a></li--><?php }?>

<!--<li><a href="?pid=57&action=mailtoadmin">Admin</a></li>-->
</ul>

<?php }}if (in_array('23_p', $top_level_permissions) ){if (in_array('23_p', $admin_permissions)){?>
<!--div class="menuheader" style="cursor: default">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td width="37px"><img src="images/help_32.png" /></td>
    <td ><a href="?pid=53&action=view" class="mainsidelink">
Mesa de ayuda</a></td>
  </tr>
</table></div-->

<?php }}if (in_array('24_p', $top_level_permissions) ){if (in_array('24_p', $admin_permissions)){?>
<!--div class="menuheader" style="cursor: default">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td width="37px"><img src="images/thought_32.png" /></td>
    <td ><a href="?pid=52&action=tip_day" class="mainsidelink">
Pensamiento de hoy</a></td>
  </tr>
</table></div-->
<?php } }if (in_array('25_p', $top_level_permissions) ){if (in_array('25_p', $admin_permissions)){?>
<?php ?><!--div class="menuheader" style="cursor: default">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td width="37px"><img src="images/photo_gallery_32.png" /></td>
    <td ><a href="?pid=54&action=albumlist" class="mainsidelink">
Album de fotos</a></td>
<?php /*?>	<td width="37px"><img src="images/photo_gallery_32.png" /></td>
    <td ><a href="?pid=116&action=abcd" class="mainsidelink">Photo Album1</a></td>
<?php */?>  </tr>
</table></div--><?php ?>
<?php }}if (in_array('26_p', $top_level_permissions) ){if (in_array('26_p', $admin_permissions)){?>
<?php ?><!--div class="menuheader" style="cursor: default">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td width="37px"><img src="images/video_32.png" /></td>
    <td ><a href="?pid=56&action=gallerylist" class="mainsidelink">Videos</a></td>
  </tr>
</table></div-->
<?php ?><?php }}if (in_array('27_p', $top_level_permissions) ){if (in_array('27_p', $admin_permissions)){?>
<!--div class="menuheader" style="cursor: default">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td width="37px"><img src="images/holiday_32.png" /></td>
    <td ><a href="?pid=58&action=holidayslist" class="mainsidelink">
Días festivos</a></td>
  </tr>
</table></div-->
<?php }}if (in_array('35_p', $top_level_permissions) ){if (in_array('35_p', $admin_permissions)){?>
<!--div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/addons_32.png" /></td>
    <td >
Enlaces Útiles</td>
  </tr>
</table></div-->
<ul class="categoryitems">
<?php if (in_array('35_1', $admin_permissions)){?>
<li><a href="?pid=93&action=addnew">Añadir enlace</a></li>
<?php }?>
<li><a href="?pid=93&action=list"> 
Ver enlaces</a></li>
</ul>
<?php }}if (in_array('28_p', $top_level_permissions) ){if (in_array('28_p', $admin_permissions)){?>
<!--div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/security_32.png" /></td>
    <td >Seguridad</td>
  </tr>
</table></div-->
<ul class="categoryitems">
<li><a href="?pid=26&action=vehicle">Registro de visitantes</a></li>
<li><a href="?pid=26&action=report"> 
Reporte</a></li>
</ul>

<?php }}
if (in_array('30_p', $top_level_permissions) ){if (in_array('30_p', $admin_permissions)){?>
<!--div class="menuheader expandable"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="37px"><img src="images/knowledge_base_32.png" /></td>
    <td >
Base de conocimientos</td>
  </tr>
</table></div-->
<ul class="categoryitems">
<li><a href="?pid=30&action=know_category">
Crear categoría</a></li>
<li><a href="?pid=30&action=know_categ">Buscar artículos</a></li>
</ul>
<?php }} if (in_array('31_p', $top_level_permissions) ){if (in_array('31_p', $admin_permissions)){?>
<!--div class="menuheader" style="cursor: default">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td width="37px"><img src="images/notice_board_32.png" /></td>
    <td ><a href="?pid=37&action=noticeboard" class="mainsidelink">
Tablón de anuncios</a></td>
  </tr>
</table></div-->
 <?php }}?>

 <?php if (in_array('34_p', $top_level_permissions) ){if (in_array('34_p', $admin_permissions) && in_array('34_1', $admin_permissions)){?>
<!--div class="menuheader" style="cursor: default">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td width="37px"><img src="images/roll_management.png" /></td>
    <td ><a href="?pid=91&action=report" class="mainsidelink">
Gestión de roles</a></td>
  </tr>
</table></div-->
 <?php }}?>


</div></td>
  </tr>
</table>

